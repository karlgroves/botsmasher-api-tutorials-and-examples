<?php
/**
 * This file provides a PHP-based example of using the BotSmasher API to check for
 * a "bad guy" in the BotSmasher system.  In this use case, you have gathered the
 * IP, name, and email of a system user and want to know if they may be a bad guy
 *
 * Upon successful submission of this API request, you will know whether this user
 * already exists in our system (or in your specific block list)
 */

// URL for API requests
$apiURL = 'https://www.botsmasher.com/api/';

// put your key here
$botSmasherAPIKey = '';

// MUST be 'submit', 'clear' or 'check'
$opts['action'] = 'submit';

// Enter a valid IP in AAA.BBB.CCC.DDD format. Invalid IPs get rejected
$opts['ip'] = '127.0.0.1';

// Enter a valid email address here. Invalid emails get rejected
$opts['email'] = 'me@example.com';

// Name can be any arbitrary string
$opts['name'] = 'Joe Schmoe';

// Create a new instance of the botsmasherClient class
$botSmasher = new botsmasherClient($apiURL, $botSmasherAPIKey);

// Set the options
$botSmasher -> setOpts($opts);

// Send the request
if (FALSE == $botSmasher -> sendRequest()) {
    echo 'REQUEST FAILED';
    exit ;
}

// Decode the response
$response = $botSmasher -> decode();

// If botSmasher::decode returns false, it is because the JSON could not be decoded.
if (FALSE == $response) {
    echo ' ERROR: NOT ABLE TO DECODE THE RESPONSE ';
    exit ;
} else {
    if ($response['response']['summary']['code'] == 'failure') {
        throw new Exception('BAD REQUEST: ' . $response['response']['summary']['description']);
    } elseif ($response['response']['summary']['code'] == 'success') {
        if ($response['response']['summary']['badguys'] == 'true') {
            echo 'BAD GUY FOUND';
        } else {
            echo 'NO BAD GUYS';
        }
    }
}
?>