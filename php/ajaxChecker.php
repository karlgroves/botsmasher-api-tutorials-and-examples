<?php
/**
 * Call this page whenever a user lacks an appropriate cookie value
 * It will check the user against BotSmasher API and set a cookie if they're ok.
 * If they're not OK, set a different cookie (or take other action)
 */

require ('botsmasherClient.class.php');
$apiURL = 'https://www.botsmasher.com/api/';
$opts['ip'] = '127.0.0.1';

// Create a new instance of the botsmasherClient class
$botSmasher = new botsmasherClient($apiURL, $botSmasherAPIKey);

// Set the options
$botSmasher -> setOpts($opts);

// Send the request
if (FALSE == $botSmasher -> sendRequest()) {
    echo 'REQUEST FAILED';
    exit ;
}

// Decode the response
$response = $botSmasher -> decode();

// If botSmasher::decode returns false, it is because the JSON could not be decoded.
if (FALSE == $response) {
    echo ' ERROR: NOT ABLE TO DECODE THE RESPONSE ';
    exit ;
} else {
    if ($response['response']['summary']['code'] == 'failure') {
        throw new Exception('BAD REQUEST: ' . $response['response']['summary']['description']);
    } elseif ($response['response']['summary']['code'] == 'success') {
        if ($response['response']['summary']['badguys'] == 'true') {
            echo 'BAD GUY FOUND';
        } else {
            echo 'NO BAD GUYS';
        }
    }
}
?>