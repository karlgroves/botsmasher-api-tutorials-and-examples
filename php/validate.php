<?php
/**
 * This file provides a PHP-based example of using the BotSmasher API to validate
 * form submissions by submitting a 'check' request  to the BotSmasher system.
 * In this use case, you have gathered the IP, name, and email of a user via form
 * submission and send it to BotSmasher for a quick check
 *
 * Upon successful submission of this API request, you will know whether this user
 * already exists in our system (or in your specific block list)
 */

/**
 * Function to get the IP address of the user submitting the form
 * @return  string
 */
function getip() {
    if (isset($_SERVER)) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $realip = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $realip = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $realip = $_SERVER['HTTP_FORWARDED'];
        } else {
            $realip = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        if (getenv('HTTP_X_FORWARDED_FOR')) {
            $realip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_CLIENT_IP')) {
            $realip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $realip = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $realip = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $realip = getenv('HTTP_FORWARDED');
        } else {
            $realip = getenv('REMOTE_ADDR');
        }
    }
    return $realip;
}

// for the love of God, in your final implementation
// do some smarter filtering & validation of
// submitted POST information before moving forward
if (isset($_POST['submit'])) {

    // URL for API requests
    $apiURL = 'https://www.botsmasher.com/api/';

    // put your key here
    $botSmasherAPIKey = '';

    // MUST be 'submit', 'clear' or 'check'
    $opts['action'] = 'submit';

    // get the IP of the user who submitted the form
    $opts['ip'] = getip();

    // Like I said, do some more intelligent validation in real life
    $opts['email'] = $_POST['email'];

    // Like I said, do some more intelligent validation in real life
    $opts['name'] = $_POST['name'];

    // Create a new instance of the botsmasherClient class
    $botSmasher = new botsmasherClient($apiURL, $botSmasherAPIKey);

    // Set the options
    $botSmasher -> setOpts($opts);

    // Send the request
    if (FALSE == $botSmasher -> sendRequest()) {
        echo 'REQUEST FAILED';
        exit ;
    }

    // Do the validation. This method returns TRUE if no bad guys found, FALSE otherwise
    if (FALSE == $botSmasher -> validate()) {
        echo 'BAD GUYS FOUND';
        exit ;
    } else {
        echo 'NO BAD GUYS FOUND';
        exit ;
    }
}
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
	<label for="name">Name:
		<input type="text" name="name" id="name">
	</label>
	<label for="email">Email:
		<input type="email" name="email" id="email">
	</label>
	<input type="submit" name="submit" value="Submit">
</form>
