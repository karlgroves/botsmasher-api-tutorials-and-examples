<?php
/**
 * Sample PHP client class to query the BotSmasher API to check
 * an IP, email, or name is found common to spammers
 *
 *
 * USAGE EXAMPLE:
 * $apiURL = 'https://www.botsmasher.com/api/';
 * $botSmasherAPIKey = 'ENTER YOUR KEY HERE';
 *
 * $botSmasher = new botsmasherClient($apiURL, $botSmasherAPIKey);
 *
 * $opts['action'] = 'check';
 * $opts['ip'] = '127.0.0.1';
 * $opts['email'] = 'me@example.com';
 * $opts['name'] = 'Joe Schmoe';
 *
 * $botSmasher -> setOpts($opts);
 * $botSmasher -> sendRequest();
 *
 * // this method returns TRUE if no bad guys found, FALSE otherwise
 * $botSmasher -> validate();
 *
 */
class botsmasherClient {

    /**
     * class constructor
     * @param   string  $apiURL the URL to send the query to
     * @param   string  $apiKey the key for the API request
     */
    public function __construct($apiURL, $apiKey) {
        $this -> apiURL = $apiURL;
        $this -> apiKey = $apiKey;
    }

    /**
     *  Set the query parameters for request
     * @param   array   $opts array of options for the request
     */
    public function setOpts($opts) {
        foreach ($opts AS $key => $val) {
            $this -> opts[$key] = $val;
        }
    }

    /**
     * Checks the submitted information to make sure we will get
     * a good response from the API
     * @return bool
     */
    protected function validateOpts() {
        
        // 'key' is absolutely required by botsmasher
        if (FALSE == array_key_exists('key', $this -> opts)) {
            return FALSE;
        }

        // one of 'ip', 'email', or 'name' have to be set
        if ((!isset($this -> opts['ip'])) && (!isset($this -> opts['email'])) && (!isset($this -> opts['name']))) {
            return FALSE;
        }

        // action not set
        if (FALSE == array_key_exists('action', $this -> opts)){
            return FALSE;
        }
        
        // action must be 'check', 'submit', or 'clear'
        $actionVals = array('check', 'submit', 'clear');
        if(FALSE == $this -> inArrayNoCase($this -> opts['action'], $actionVals)){
            return FALSE;
        }
        
        // all API keys are 64 characters
        if (strlen(trim($this -> opts['key'])) != 64) {
            return FALSE;
        }

        // if we got this far we're GTG
        return TRUE;

    }

    /**
     * Submits the request to the API
     * @param   bool    $printInfo  whether or not to print the output from curl_getinfo (usually for debugging only)
     * @return  string  the results, formatted as JSON
     */
    public function sendRequest($printInfo = FALSE) {
        // add the api key to the query string
        $this -> opts['key'] = $this -> apiKey;

        if (FALSE == $this -> validateOpts()) {
            if (TRUE == $printInfo) {
                echo 'INVALID OPTS';
            }
            return FALSE;
        }

        //open connection
        $ch = curl_init();

        if (TRUE == $printInfo) {
            echo '<pre>';
            var_dump($this -> opts);
            echo '</pre>';
        }

        curl_setopt($ch, CURLOPT_URL, $this -> apiURL);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this -> opts);

        //execute post and get results
        $result = curl_exec($ch);

        if (TRUE == $printInfo) {
            echo curl_error($ch);
            echo '<pre>';
            print_r(curl_getinfo($ch));
            echo '</pre>';
        }

        //close connection
        curl_close($ch);

        //the results, formatted as JSON
        $this -> response = $result;
    }

    /**
     *
     * Just takes the JSON, makes sure everything was OK with it, and turns it into an array
     * @return array
     */
    public function decode() {
        // if there's no response then there's nothing to decode
        // you're probably better off throwing an exception in your final implementation.
        if ((FALSE == $this -> response) || (!isset($this -> response))) {
            return FALSE;
        }

        $array = json_decode($this -> response, TRUE);

        try {
            if (is_null($array)) {
                switch (json_last_error()) {
                    case JSON_ERROR_DEPTH :
                        $msg = ' - Maximum stack depth exceeded';
                        break;
                    case JSON_ERROR_STATE_MISMATCH :
                        $msg = ' - Underflow or the modes mismatch';
                        break;
                    case JSON_ERROR_CTRL_CHAR :
                        $msg = ' - Unexpected control character found';
                        break;
                    case JSON_ERROR_SYNTAX :
                        $msg = ' - Syntax error, malformed JSON';
                        break;
                    case JSON_ERROR_UTF8 :
                        $msg = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                        break;
                    default :
                        $msg = ' - Unknown error';
                        break;
                }
                throw new Exception($msg);
            }
        } catch (Exception $e) {
            // echo-ing the message might not be the best approach for your final implementation.
            // log it, send it to yourself via email, or something
            echo $e -> getMessage();
            return FALSE;
        }
        return $array;
    }

    /**
     * This function does validation
     * @return  bool    FALSE if bad guys were found, TRUE if all is well.
     */
    public function validate() {
        try {
            $array = $this -> decode();

            if (FALSE == $array) {
                throw new Exception(' ERROR: NOT ABLE TO DECODE THE RESPONSE ');
            } else {
                if ($array['response']['summary']['code'] == 'failure') {
                    throw new Exception('BAD REQUEST: ' . $array['response']['summary']['description']);
                } elseif ($array['response']['summary']['code'] == 'success') {
                    if ($array['response']['summary']['badguys'] == 'true') {
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                }
            }
        } catch (Exception $e) {
            echo $e -> getMessage();
            return FALSE;
        }
    }

    /**
     *
     * searches for an item in an array, without being case sensitive
     * @param   string  $search     The item we're checking for
     * @param   array   &$array     The array we're looking in
     * @return  bool
     */
    public function inArrayNoCase($search, $array) {
        if (Arrays::isArray($array, false)) {
            $search = strtolower($search);
            foreach ($array as $item) {
                if (strtolower($item) == $search) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }
    }

}
?>